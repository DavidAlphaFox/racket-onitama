# About racket-onitama
This is a little project using [Racket](https://racket-lang.org/) to create an implementation of [Onitama](https://www.arcanewonders.com/game/onitama/).  

# Screenshot
![start](images/screenshot.png "Start of game")

# Download
Downloads are provided as compressed files. Extract somewhere and run the executable.  
[Windows version](https://gitlab.com/DustinCSWagner/racket-onitama/raw/master/distributions/onitama-win.zip)  
[Linux version](https://gitlab.com/DustinCSWagner/racket-onitama/raw/master/distributions/onitama-linux.tgz)  

# Playing Onitama
There are two players (red and blue).
Each player has four 'pawns' and one 'king'.
To win, capture the other players king or get your king to their king's starting square. 
Each player is dealt two movement cards.  
Either of the cards can be applied to any of the player's pieces.  
When you use a card to move, it is exchanged with the swap card (located between the players).  
The other player will get that card on their next turn.  
The starting player is determined at random by the color on the swap card.  
You can play with two humans, human vs IA (intelligent agent), and IA vs IA.  
Hit 'r' for a random move or use one of the function keys for a more intelligent move.  
(F1 is the fastest and weakest IA, F12 is the strongest, try F5 for a decent IA).  
As a human, click on the card, piece, and destination you want then click 'confirm'.  
You could then hit a key for an IA move, play the other player yourself, or have a friend make a move.  
Note that there is a red/blue turn indicator on the right side of the screen.  

# Thanks
Thanks to Shimpei Sato and the team behind Onitama for creating a really cool game. Go buy a copy!

# Other
[Boardgamegeek](https://boardgamegeek.com/boardgame/160477/onitama)  
Minimum screen resolution: 1200 x 600  
License is [LGPLv3](https://gitlab.com/DustinCSWagner/racket-onitama/raw/master/LICENSE)
 :thumbsup:




